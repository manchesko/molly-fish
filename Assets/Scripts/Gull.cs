﻿using UnityEngine;
using System.Collections;

public class Gull : MonoBehaviour
{

    [SerializeField] private float speed;
    [SerializeField] Rigidbody2D gull;
    [SerializeField] private float spawnY;
    [SerializeField] private float spawnX;

    void Start()
    {
        StartCoroutine(Inst());
    }

    public void OnEnable()
    {
        //float y = spawn;
        transform.position = new Vector2(spawnX, spawnY);
        gull.AddForce(new Vector2(-speed, spawnY));
    }

    IEnumerator Inst()
    {
        yield return new WaitForSeconds(3f);
        GameObject gu = Instantiate(gameObject, gameObject.transform.position, Quaternion.identity) as GameObject;
        Destroy(gu, 6.5f);
    }
}
