﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreText : MonoBehaviour
{
    public Text counterScore;
    public int score = 0;

    public void AddScore()
    {
        score++;
        counterScore.text = "Score: " + score.ToString();
        if (score >= 2)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
