﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlaerControler : MonoBehaviour
{
    Rigidbody2D fish;
    [SerializeField] private float jumpforce;
    [SerializeField] private float bounce;
    [SerializeField] private float bounce2;
    [SerializeField] private float bounceground;
    [SerializeField] private float bottom;
    [SerializeField] private float water;
    [SerializeField] private float sky;
    [SerializeField] private int thislevel;    
    private Quaternion targetRotation;
    public ScoreText scoreText;
    
	void Start ()
    {
        fish = GetComponent<Rigidbody2D>();   
	}

    private void Update()
    {     
        Vector3 position = transform.position;
        
        if (position.y <= bottom)
        {
            fish.AddForce(Vector2.up * bounceground, ForceMode2D.Impulse);
        }
        if (position.y >= water)
        {
            fish.AddForce(Vector2.down * bounce, ForceMode2D.Impulse);
        }
        if (position.y >= sky)
        {
            fish.AddForce(Vector2.down * bounce2, ForceMode2D.Impulse);
            //jumpforce = 0;- с целью продотвратить многоразовый джамп, не работает
        }
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            fish.AddForce(Vector2.up * jumpforce, ForceMode2D.Impulse);
        }
        

        targetRotation = Quaternion.Euler(0, 0, Mathf.Clamp(fish.velocity.y * 20, -45, 45));
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 10);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Obstacle")
        {
            SceneManager.LoadScene(thislevel);
        }
        if(collision.gameObject.tag == "Star")
        {         
            Destroy(collision.gameObject);
            scoreText.AddScore();            
        }
    }
}
