﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D star;

    [SerializeField] private float lowSpawn;
    [SerializeField] private float heightSpawn;
    [SerializeField] private float spawnX;


    void Start()
    {
        StartCoroutine(Inst());
    }

    public void OnEnable()
    {
        float y = Random.Range(lowSpawn, heightSpawn);
        transform.position = new Vector2(spawnX, y);
        star.AddForce(new Vector2(-speed, lowSpawn));
    }

    IEnumerator Inst()
    {
        yield return new WaitForSeconds(2.7f);
        GameObject st = Instantiate(gameObject, gameObject.transform.position, Quaternion.identity) as GameObject;
        Destroy(st, 7);
    }
}
