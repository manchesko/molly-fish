﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shark : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D shark;

    [SerializeField] private float heightSpawnMin;
    [SerializeField] private float heightSpawnMax;
    [SerializeField] private float spawnX;

    void Start()
    {
        StartCoroutine(Inst());
    }

    public void OnEnable()
    {
        float y = Random.Range(heightSpawnMin, heightSpawnMax);
        transform.position = new Vector2(spawnX, y);
       
        shark.AddForce(new Vector2(-speed, y));
    }

    IEnumerator Inst()
    {
        yield return new WaitForSeconds(2.3f);
        GameObject sh = Instantiate(gameObject, gameObject.transform.position, Quaternion.identity) as GameObject;
        Destroy(sh, 8);
    }
}
